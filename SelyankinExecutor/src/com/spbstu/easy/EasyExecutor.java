package com.spbstu.easy;

import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.logging.Logger;

import java.util.ArrayList;
import java.util.List;

public class EasyExecutor implements Executor {
    ArrayList<Consumer> consumers;
    Logger logger;
    byte[] data;
    Status status;

    public EasyExecutor(String config, Logger logger){
        this.consumers = new ArrayList<Consumer>();
        this.logger = logger;
        status = Status.OK;
    };

    @Override
    public void loadDataFrom(Producer producer) {
        if (!producer.status().equals(Status.OK)) {
            status = Status.EXECUTOR_ERROR;
            logger.log("Error in \"" + this.getClass().toString());
            return;
        }
        data = (byte[]) producer.get();
    }

    @Override
    public void run() {
        if (!status.equals(Status.OK)){ return; }
        process();
        for (Consumer consumer :consumers){
            consumer.loadDataFrom(this);
            consumer.run();
        }
    }

    @Override
    public void addProducer(Producer producer) {
    }

    @Override
    public void addProducers(List<Producer> list) {
    }

    @Override
    public void addConsumer(Consumer consumer) {
        consumers.add(consumer);
    }

    @Override
    public void addConsumers(List<Consumer> list) {
        consumers.addAll(list);
    }

    @Override
    public Object get() {
        return data;
    }

    @Override
    public Status status() {
        return status;
    }

    private void process(){
        for (int i = 0; i < data.length / 2; i++){
            byte tmp = data[i];
            data[i] = data[data.length - i - 1];
            data[data.length - i - 1] = tmp;
        }
    }
}

