import com.spbstu.manager.Manager;
import ru.spbstu.pipeline.logging.UtilLogger;

import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class Main {
    private static final java.util.logging.Logger LOGGER = Logger.getLogger(com.spbstu.easy.EasyExecutor.class.getName());

    static {
        FileHandler fileHandler = null;
        try {
            fileHandler = new FileHandler("./pipeline.log");
            LOGGER.addHandler(fileHandler);
            fileHandler.setFormatter(new SimpleFormatter());
            fileHandler.setLevel(Level.ALL);
            LOGGER.setLevel(Level.ALL);
        } catch (Exception exception) {
            LOGGER.log(Level.SEVERE, "Error occur in FileHandler.", exception);
        }
    }

    public static void main(String[] args) {
        ru.spbstu.pipeline.logging.Logger logger = UtilLogger.of(LOGGER);
        Manager manager = new Manager(args[0], logger);
        manager.run();
    }
}
