package com.spbstu.config;

import ru.spbstu.pipeline.Status;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

public class Config {
    enum Grammar {
        G_INPUT("INPUT"),
        G_OUTPUT("OUTPUT"),
        G_EXECUTOR("EXECUTOR"),
        G_WORDLEN("WORDLEN"),
        G_SEP("="),
        G_SPACE("\\s");

        private String keyWord;

        public String toString() {
            return this.keyWord;
        }

        Grammar(String keyWord) {
            this.keyWord = keyWord;
        }
    }

    enum DefaultValue {
        WORD_LENGTH(8),
        EXECUTOR_WORDS_COUNT(3);

        private final int value;

        DefaultValue(int value) {
            this.value = value;
        }
    }

    private Status status = Status.OK;
    private String inputFileName = "";
    private String outputFileName = "";

    //size of buffer to be exchange by executors
    private int readLength = DefaultValue.WORD_LENGTH.value;

    //Executors class names
    private ArrayList<String> execNames = new ArrayList<String>();

    //Executors config file names
    private ArrayList<String> execConfigsFiles = new ArrayList<String>();

    public Status status() {
        return status;
    }

    public int getReadLength() {
        return readLength;
    }

    public String getInputFileName() {
        return inputFileName;
    }

    public String getOutputFileName() {
        return outputFileName;
    }

    public ArrayList<String> getExecNames() {
        return execNames;
    }

    public ArrayList<String> getExecConfigsFilesNames() {
        return execConfigsFiles;
    }

    /**
     * open and parse config file
     *
     * @param filename - name of config file for manager
     */
    public void buildFromFile(String filename) {
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new FileReader(filename));
            parseFile(bufferedReader);
        } catch (Exception e) {
            status = Status.ERROR;
        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (Exception e) {
                status = Status.ERROR;
            }
        }
    }

    /**
     * parse config file and
     *
     * fills in the fields specified in the config file
     * @param bufferedReader
     */
    private void parseFile(BufferedReader bufferedReader) {
        try {
            for (String line = bufferedReader.readLine(); line != null; line = bufferedReader.readLine()) {
                line = line.replace(Grammar.G_SPACE.toString(), "");
                if (!line.contains(Grammar.G_SEP.toString())) {
                    status = Status.ERROR;
                    return;
                }
                String[] words = line.split(Grammar.G_SEP.toString());

                if (Grammar.G_INPUT.toString().equals(words[0])) {
                    inputFileName = words[1];
                } else if (Grammar.G_OUTPUT.toString().equals(words[0])) {
                    outputFileName = words[1];
                } else if (Grammar.G_EXECUTOR.toString().equals(words[0])) {
                    if (words.length < DefaultValue.EXECUTOR_WORDS_COUNT.value) {
                        status = Status.ERROR;
                        break;
                    }
                    execNames.add(words[1]);
                    execConfigsFiles.add(words[2]);
                } else if (Grammar.G_WORDLEN.toString().equals(words[0])) {
                    readLength = new Integer(words[1]);
                }
            }
        } catch (IOException e) {
            status = Status.ERROR;
        }
    }
}
