package com.spbstu.manager;

import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.logging.Logger;

import java.util.ArrayList;

public class easyFactory {
    /**
     * get instance of class
     *
     * @param className - class name the instance of which you want to create
     * @param configFilename - config file name the instance of which you want to create
     * @param logger - logger
     * @return class instance
     */
    private static Executor getInstance(String className, String configFilename, Logger logger){
        Executor execInstance;
        try {
            execInstance = (Executor) Class.forName(className)
                    .getConstructor(new Class[]{String.class, Logger.class})
                    .newInstance(configFilename, logger);
        } catch (Exception e){
            return null;
        }

        return execInstance;
    }

    public static EasyReader getReader(String inputFileName, int readLength){
        return new EasyReader(inputFileName, readLength);
    }

    public static EasyWriter getWriter(String outputFileName){
        return new EasyWriter(outputFileName);
    }

    /**
     * @param execNames list of executors class name
     * @param execConfigsFiles - list of config file for executors
     * @param logger - logger
     * @return - list of executors instance
     */
    public static ArrayList<Executor> getExecutorList(ArrayList<String> execNames, ArrayList<String> execConfigsFiles,Logger logger){
        ArrayList<Executor> executorsList = new ArrayList<Executor>();

        for (int i = 0; i < execNames.size(); i++){
            Executor inst = getInstance(execNames.get(i), execConfigsFiles.get(i), logger);
            if (inst != null){
                executorsList.add(inst);
            }
        }
        return executorsList;
    }
}
