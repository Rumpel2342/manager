package com.spbstu.manager;

import com.spbstu.config.Config;
import ru.spbstu.pipeline.Executor;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.logging.Logger;

import java.util.ArrayList;

public class Manager implements Runnable {
    private String managerConfigFileName;
    private Config config;
    private Logger logger;

    public Manager(String managerConfigFileName, Logger logger){
        config = new Config();
        this.managerConfigFileName = managerConfigFileName;
        this.logger = logger;
    }

    @Override
    public void run() {
        config.buildFromFile(managerConfigFileName);
        if (!config.status().equals(Status.OK)){
            logger.log("fatal error while parse manager's config file");
            return;
        }

        EasyReader reader = easyFactory.getReader(config.getInputFileName(), config.getReadLength());
        if (!reader.status().equals(Status.OK)){
            logger.log("Can not open input file");
            return;
        }

        EasyWriter writer = easyFactory.getWriter(config.getOutputFileName());
        if (!writer.status().equals(Status.OK)){
            logger.log("Can not write to output file");
            reader.close();
            return;
        }

        ArrayList<Executor> execInstances = easyFactory.getExecutorList(config.getExecNames(), config.getExecConfigsFilesNames(), logger);
        if (!config.status().equals(Status.OK)){
            logger.log("fatal error while creating executors instance");
            reader.close();
            writer.close();
            return;
        }
        if (execInstances.get(0) != null){
            reader.addConsumer(execInstances.get(0));
            for (int i = 0; i < execInstances.size() - 1; i++){
                execInstances.get(i).addConsumer(execInstances.get(i+1));
            }
            execInstances.get(execInstances.size()-1).addConsumer(writer);
        }
        else {
            reader.addConsumer(writer);
        }
        reader.run();
        reader.close();
        writer.close();
    }
}
