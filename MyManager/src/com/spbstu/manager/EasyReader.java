package com.spbstu.manager;

import ru.spbstu.pipeline.Consumer;
import ru.spbstu.pipeline.Reader;
import ru.spbstu.pipeline.Status;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class EasyReader implements Reader {
    private byte[] data;
    int readLength;
    private ArrayList<Consumer> consumers;
    private Status status;
    private FileReader fileReader;

    @Override
    public void run() {
        char[] readCharBuf = new char[readLength];
        try {
            for (int realread = fileReader.read(readCharBuf, 0, readLength);
                 realread > 0;
                 realread = fileReader.read(readCharBuf, 0, readLength)){
                data = new String(readCharBuf).substring(0, realread).getBytes();
                for (Consumer consumer : consumers){
                    consumer.loadDataFrom(this);
                    consumer.run();
                }
            }
        } catch (Exception e) {
            status = Status.READER_ERROR;
        }
    }

    public EasyReader(String filename, int readLength) {
        this.readLength = readLength;
        this.status = Status.OK;
        this.consumers = new ArrayList<Consumer>();
        try {
            fileReader = new FileReader(filename);
        } catch (FileNotFoundException e) {
            status = Status.READER_ERROR;
        }
    }

    @Override
    public void addConsumer(Consumer consumer) {
        consumers.add(consumer);
    }

    @Override
    public void addConsumers(List<Consumer> list) {
        consumers.addAll(list);
    }

    @Override
    public Object get() {
        return data;
    }

    @Override
    public Status status() {
        return status;
    }

    public void close(){
        try {
            fileReader.close();
        } catch (IOException e) {
            status = Status.READER_ERROR;
        }
    }
}
