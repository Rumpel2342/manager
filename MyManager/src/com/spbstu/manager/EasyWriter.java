package com.spbstu.manager;

import ru.spbstu.pipeline.Producer;
import ru.spbstu.pipeline.Status;
import ru.spbstu.pipeline.Writer;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class EasyWriter implements Writer {
    FileWriter fileWriter;
    Status status;
    byte[] data;

    public EasyWriter(String filename){
        try {
            fileWriter = new FileWriter(filename);
        } catch (IOException e) {
            status = Status.WRITER_ERROR;
        }
        this.status = Status.OK;
    }

    @Override
    public void loadDataFrom(Producer producer) {
        if (!producer.status().equals(Status.OK)) {
            status = Status.EXECUTOR_ERROR;
        }
        data = (byte[]) producer.get();
    }

    @Override
    public void run() {
        try {
            fileWriter.write(new String(data));
        } catch (IOException e) {
            status = Status.WRITER_ERROR;
        }
    }

    @Override
    public Status status() {
        return status;
    }

    @Override
    public void addProducer(Producer producer) {

    }

    @Override
    public void addProducers(List<Producer> list) {

    }

    public void close(){
        try {
            fileWriter.close();
        } catch (IOException e) {
            status = Status.WRITER_ERROR;
        }
    }
}
